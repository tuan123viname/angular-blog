export interface User{
  name : String,
  email : String,
  phone : String,
  avatar: String,
  accessToken : String
}
