import { Component, OnInit, DoCheck } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, DoCheck {
  tabActive = 'tab1';
  handleTabActive(tab){
    this.tabActive = tab;
  }
  router : Router;
  user : User;
  constructor( _authService : AuthService, router : Router ) {
    _authService.user$.subscribe(user => this.user = user);
    this.router = router;
  }

  ngOnInit(): void {

  }
  ngDoCheck(): void{
    this.user.accessToken ? null  : this.router.navigate(['/']);
  }

}
