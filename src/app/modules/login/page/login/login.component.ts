import { Component, OnInit, OnChanges, DoCheck } from '@angular/core';
import { faUser , faKey} from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../../../services/auth/auth.service';
import { User } from 'src/app/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnChanges, DoCheck {
  faUser = faUser;
  faKey = faKey;

  loginWithFacebook;
  loginWithGoogle;
  user : User;
  router;
  constructor( _authService : AuthService, router : Router) {
    this.loginWithFacebook = _authService.loginWithFacebook;
    this.loginWithGoogle = _authService.loginWithGoogle;
    _authService.user$.subscribe(user => this.user = user);
    this.router = router;
   }

  ngOnInit(): void {
  }
  ngOnChanges():void{

  }
  ngDoCheck() : void {
    this.user.accessToken ? this.router.navigate(['/']) : null;
  }

}
