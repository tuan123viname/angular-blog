import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SharedModule } from '../../shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { PosterCarouselComponent } from './components/poster-carousel/poster-carousel.component';
import { HomeRoutingModule } from './home-routing.module';
import { PostCategoriesComponent } from './components/post-categories/post-categories.component';
import { PostTagsComponent } from './components/post-tags/post-tags.component';


@NgModule({
  imports : [ CommonModule, CarouselModule , HomeRoutingModule , SharedModule],
  declarations : [HomeComponent, PosterCarouselComponent, PostCategoriesComponent, PostTagsComponent],
  exports : []
})

export class HomeModule {};
