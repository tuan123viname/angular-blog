import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

interface poster{
  image : string,
  title : string,
  author: string,
  id: string,
  description: string,
  dateRelease : string,
  avt: string
}

@Component({
  selector: 'app-poster-carousel',
  templateUrl: './poster-carousel.component.html',
  styleUrls: ['./poster-carousel.component.scss']
})
export class PosterCarouselComponent implements OnInit {
  list : poster[];
  constructor() {
    this.list = [
      {
      image : 'https://advancedfineart.com/wp-content/uploads/2018/09/southport-pier-nc-sunset_ocean_lee-filters_big-stopper_20180901_001_4B5A7928-HDR-Edit.jpg',
      author : 'vu ngoc tuan',
      dateRelease : '11/06/1999',
      avt : 'https://picsum.photos/50/50',
      description : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.Lorem ipsum dolor sit amet consectetur, adipisicing elit.Lorem ipsum dolor sit amet consectetur, adipisicing elit.Lorem ipsum dolor sit amet consectetur, adipisicing elit ',
      id: 'dslfsjd',
      title : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.'
    },
    {
      image : 'https://i.pinimg.com/originals/53/87/0f/53870f98b9545ddf0f1fd20ad17c6076.jpg',
      author : 'vu ngoc tuan',
      dateRelease : '11/06/1999',
      avt : 'https://picsum.photos/50/50',
      description : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.Lorem ipsum dolor sit amet consectetur, adipisicing elit.',
      id: 'dslfsjddfdf',
      title : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.'
    },
    {
      image : 'https://i.pinimg.com/originals/53/87/0f/53870f98b9545ddf0f1fd20ad17c6076.jpg',
      author : 'vu ngoc tuan',
      dateRelease : '11/06/1999',
      avt : 'https://picsum.photos/50/50',
      description : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.Lorem ipsum dolor sit amet consectetur, adipisicing elit.',
      id: 'dslfsjddfdf',
      title : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit.'
    }
  ]
  }

  ngOnInit(): void {
  }

  customOptions : OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 2
      }
    },


  }


}
