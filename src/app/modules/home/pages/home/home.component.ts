import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  activeTab = 'popular';
  user : User;
  handleActiveTab(tab){
    this.activeTab = tab;
  }

  constructor(_authService : AuthService) {
    _authService.user$.subscribe(user => this.user = user);
   }

  ngOnInit(): void {
  }

}
