import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { NewArticleComponent } from './pages/new-article/new-article.component';

const routes : Routes = [
  {
    path : '',
    component : ArticleDetailComponent
  },
  {
    path : 'them-bai-viet',
    component : NewArticleComponent
  }
];


@NgModule({
  imports : [ RouterModule.forChild(routes)],
  exports : [ RouterModule ]
})

export class ArticleRoutingModule {};
