import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.scss']
})
export class NewArticleComponent implements OnInit {
  thumbnail : String;
  handleChangeImage(event : any){
    console.log(event);
    this.thumbnail = URL.createObjectURL(event.target.files[0]);
  }
  constructor() { }
  ngOnInit(): void {
    $('#summernote').summernote(
      {
        height: 500,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true,
        callbacks : {
          onImageUpload :  function(files, editor, $editable) {

            const data = new FormData();
            $('#summernote').summernote("insertImage", URL.createObjectURL(files[0]), 'filename');
              data.append("file", files[0]);
              $.ajax({
              url: "uploader.php",
              data: data,
              cache: false,
              contentType: false,
              processData: false,
              type: 'POST',
              success: function(data){
              alert(data);

            },
              error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus+" "+errorThrown);
              }
            });
            }
        }
      }
    );

  }

  getContent(){
    var markupStr = $('#summernote').summernote('code');
    console.log(markupStr);
  }

}
