import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { ArticleRoutingModule } from './article-routing.module';
import { ArticleContentComponent } from './components/article-content/article-content.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NewArticleComponent } from './pages/new-article/new-article.component';
import { CommonModule } from '@angular/common';


@NgModule({
  imports : [ ArticleRoutingModule , SharedModule, FormsModule , CommonModule ],
  declarations : [ArticleDetailComponent, ArticleContentComponent, NewArticleComponent],
  exports : []
})

export class ArtilceModule {};
