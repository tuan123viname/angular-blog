import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentLayoutComponent } from './layout/content-layout/content-layout.component';
import { LoginComponent } from './modules/login/page/login/login.component';


const routes: Routes = [
  {
    path : '',
    component : ContentLayoutComponent,
    children : [
      {
        path : '',
        loadChildren : () =>
        import('./modules/home/home.module').then(m => m.HomeModule)
      },
      {
        path : 'bai-viet',
        loadChildren : () =>
        import('./modules/article/article.module').then(m=> m.ArtilceModule)
      },
      {
        path : 'profile',
        loadChildren : () =>
        import('./modules/profile/profile.module').then(m => m.ProfileModule)
      }
    ]
  },
  {
    path : 'login',
    component : LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
