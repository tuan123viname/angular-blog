import { Injectable } from '@angular/core';
import { User } from '../../interfaces/';

import * as firebase from 'firebase';
import { auth } from 'firebase';
import {  BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user : BehaviorSubject<User> = new BehaviorSubject<User>({
    name : '',
    phone : "",
    email : '',
    avatar : '',
    accessToken : ''
  });
  user$ : Observable<User> = this.user.asObservable();

  constructor() {
    firebase.initializeApp({
      apiKey: "AIzaSyAp9TZR6GbmQt9JvOdvzoQWcyhfmdfqKTQ",
      authDomain: "myblog-45aab.firebaseapp.com",
      databaseURL: "https://myblog-45aab.firebaseio.com",
      projectId: "myblog-45aab",
      storageBucket: "myblog-45aab.appspot.com",
      messagingSenderId: "425162556419",
      appId: "1:425162556419:web:1b9a304fcabb5245fcfa36",
      measurementId: "G-T6XR0S2KLY"
    })
  }

  loginWithFacebook = () => {
    const provider = new auth.FacebookAuthProvider();
    provider.addScope('user_birthday');
    auth().useDeviceLanguage();
    provider.setCustomParameters({
      'display' : 'popup'
    });
    auth().signInWithPopup(provider)
    .then((result) => {
      const token = (<any>result).credential.accessToken;
      const userData = result.user;
      this.user.next({
        name : userData.displayName,
        email: userData.email,
        avatar : userData.photoURL,
        phone : userData.phoneNumber || '',
        accessToken : token
      })
    })
    .catch(err => {
      console.log(err);
    })
  }

  loginWithGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    firebase.auth().signInWithPopup(provider).then((result) =>  {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const token = (<any>result).credential.accessToken;
      // The signed-in user info.
      const userData = result.user;
      console.log(token);
      console.log(userData);
      this.user.next({
        name : userData.displayName,
        email: userData.email,
        avatar : userData.photoURL,
        phone : userData.phoneNumber || '',
        accessToken : token
      })

    }).catch(function(error) {
      console.log(error);

    });
  }

}
