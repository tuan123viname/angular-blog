import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostItemComponent } from './components/post-item/post-item.component';
import { AuthorCardComponent } from './components/author-card/author-card.component';

@NgModule({
  imports : [ FontAwesomeModule ],
  declarations : [ PostItemComponent, AuthorCardComponent  ],
  exports : [ PostItemComponent, AuthorCardComponent ]
})

export class SharedModule {};
