import { Component, OnInit } from '@angular/core';
import { faSearch, faBars, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/interfaces';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faSearch = faSearch;
  faBars = faBars;
  faArrowUp = faArrowUp;
  toggleInput = false;
  isOpenMenu = false;

  user : User;
  scrollTop(){
    window.scrollTo(0,0);
  }

  setIsOpenMenu(){
    this.isOpenMenu =  !this.isOpenMenu;
  }
  toggle(){
    this.toggleInput = !this.toggleInput;
  }
  constructor( _authService : AuthService ) {
    _authService.user$.subscribe(user => this.user = user);
  }

  ngOnInit(): void {
  }

}
