import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  isSticky = false;
  constructor() {

  }

  ngOnInit(): void {
     window.addEventListener('scroll', (e) => {
      if(document.documentElement.scrollTop + 20  > document.getElementById('navTop').offsetHeight)
      {
        this.isSticky = true;
      }
      else{
        this.isSticky = false;
      }
    })
  }

}
